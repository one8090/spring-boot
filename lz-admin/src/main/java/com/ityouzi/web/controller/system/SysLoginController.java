package com.ityouzi.web.controller.system;

import com.ityouzi.constant.Constants;
import com.ityouzi.core.domain.AjaxResult;
import com.ityouzi.core.domain.model.LoginBody;
import com.ityouzi.web.service.SysLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录验证
 *
 * @author lizhen created on 2022-02-11 16:18
 * @version 1.0
 */
@RestController
public class SysLoginController {

    @Autowired
    private SysLoginService loginService;

    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginBody loginBody)
    {
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    @GetMapping("/user")
    public AjaxResult getUser(){
        return new AjaxResult();
    }

    @PreAuthorize("@ss.hasPermi('system:dept:list')")
//    @PreAuthorize("@ss.hasAnyPermi('admin')")
    @GetMapping("/server")
    public AjaxResult server(){
        return new AjaxResult();
    }
}
