package com.ityouzi.mapper;

import com.ityouzi.core.domain.entity.SysUser;

public interface SysUserMapper {

    SysUser selectUserByUserName(String userName);
}