package com.ityouzi.service;

import com.ityouzi.core.domain.entity.SysUser;

/**
 * @author lizhen created on 2022-02-11 17:21
 */
public interface ISysUserService {


    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public SysUser selectUserByUserName(String userName);
}
